var Controller = {
    
    /**
     * Render the view with model data
     * 
     * @param the view to render
     */
    render:function(view){
        if(view===undefined||view===null)return;
      try {
          view.render();
      } catch (error) {
          App.context.console.log("Error rendering view (" + error + ")");
      }
    },
    /**
     * Validation for FirstName value
     * 
     * @param the FirstNAme to validate
     * @return a string with the validation error or a empty string if ok
     */
    validateFirstName:function(firstName){
        if(firstName===undefined)return "FirstName cannot be undefined";
        if(firstName.length < 3)return "FirstName must be a 3 char string (min)";
        if(firstName.lenght > 100)return "FirstName cannot be larger than 100 characters";
        return "";        
    },
    /**
     * Add a user to the users collection.
     * 
     * @param the first name of the user to be created (if undefined function will prompt it)
     * @param the view to render
     * @return a string with the error (a empty string if the user has been added correctly)
     */
    addUser:function(firstName,view){
        var result="result";
        if(firstName===undefined||firstName==""||firstName===null){        
            while(result!=""){
                firstName = App.context.window.prompt("Enter the first name of the user:")
                if(firstName===null)return ""; // user cancelled
                result = App.controller.validateFirstName(firstName);
                if(result.length>0)
                    App.context.window.alert(result);// showing controller error
            }
        }
        // Validating input
        if(App.controller.validateFirstName(firstName).length>0)
            return App.controller.validateFirstName(firstName)
        // Creating user
        App.model.users.add(firstName);
        App.context.console.log("User " + firstName + " added");
        //App.view.refresh()
        this.render(view);
        return "";
    },
    
    /**
     * Removes a user from the collection.
     * 
     * @param the id of the user to 
     * @param the view to render
     * @param true for removal confirmation
     * @return true if the user has been deleted, false if not
     */
    removeUser:function(id, view, prompt){
        if(id===undefined||id===null)return;
        if(prompt===undefined){
            App.model.users.remove(id);
            App.context.console.log("User [" + id + "] removed");
            return true;            
        }
        if(confirm("Are you sure you want to remove '" + App.model.users.get(id).firstName + "'?")){
            App.model.users.remove(id);
            App.context.console.log("User [" + id + "] removed");
            //App.view.refresh();
            this.render(view);
            return true;
        }else{
            return false;
        }
    },
    
    /**
     * Modifies the FirstName for the id
     * 
     * @param the id of the user to modify
     * @param the new FirstName to be set
     * @param the view to render
     * @return a string with the validation error or a empty string if ok
     */
    modifyUser:function(id,firstName,view){
        if(id===undefined||id===null)return "id must be a number";
        var item = App.model.users.get(id);
        if(!item)return "no item in collection";
        var uName = App.model.users.get(id).firstName;
        if(firstName===undefined)
            firstName=prompt("Enter the new FirstName for '" + uName + "'");
        // Validating input
        if(App.controller.validateFirstName(firstName).length>0)
            return App.controller.validateFirstName(firstName)
        App.model.users.get(id).firstName = firstName;
        App.context.console.log("User [" + id + "] modified");    
        //App.view.refresh();
        this.render(view);
        return "";    
    }
}