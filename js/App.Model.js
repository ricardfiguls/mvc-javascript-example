var Model = {
    Collection:{users:Array()},
        /**
         * Return all users
         */
        getUsers:function(){return this.Collection.users;},
        /**
         * Users model example to be used within bbdd sync.
         * This layer doesn't have validations
         */
        users:{
            /**
             * Add a user to the users collection and returns the created user
             * 
             * @param the firstName of the new user
             * @return a reference to the created user
             */
            add:function(firstName){
                var _user = new Dto.user(firstName);
                App.model.Collection.users.push(_user);
                return(_user);
                },
            /**
             * Count a users collection
             * 
             * @return the collection length
             */
            count:function(){
                return App.model.Collection.users.length
                },
            /**
             * Return a user by collection position
             * 
             * @param the collection position of the user
             * @return the user
             */
            get:function(id){
                return App.model.Collection.users[id]
                },
            /**
             * Remove a user from the collection
             * 
             * @param the collection position of the user to be deleted
             */
            remove:function(id){
                    App.model.Collection.users.splice(id,1)
                }
        }
        
        
    }