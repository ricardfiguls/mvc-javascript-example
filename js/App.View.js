var View = {

    /**
     * Initialize view
     */
    init:function(){
        // Adding events to User Interface
        this.get("addUser").onclick=function(){App.view._private.addUser()};
        this.get("remove").onclick=function(){App.controller.removeUser(App.view.get("users").value, App.view, true);};
        this.get("modify").onclick=function(){App.view._private.modifyUser();};
        this.get("firstName").onkeypress=function(code){App.view._private.keyPress(code);};
        this.get("list").onclick=function(li){App.view._private.listClick(li);};        
        // rendering the view
        this.render();
    },
    /**
     * Refresh view with model data
     */
    render:function(){
        // populating list & select with model data
        App.view._private.populateSelect("users",App.model.getUsers(),"firstName");
        App.view._private.populateList("list",App.model.getUsers(),"firstName");
        // cleaning input & output
        this.get("firstName").value="";
        this.get("firstName").focus();
        this.get("error").innerHTML="";
        // enabling/disabling action buttons 
        if(this.get("users").value===undefined||this.get("users").value==""){
            this.get("remove").disabled=true;
            this.get("modify").disabled=true;
        }else{
            this.get("remove").disabled=false;
            this.get("modify").disabled=false;            
        }
    },
    
    /**
     * Returning the View object from initial App context
     */
    get:function(id){return App.context.document.getElementById(id)},
    
    /**
     * 'private' subs and functions
     */
    _private:{
        parseHtml:function(input){
          var output;
          output = input.replace(new RegExp("&", "g"), "&amp;");
          output = output.replace(new RegExp("<", "g"), "&lt;");
          return output;  
        },
        populateList:function(listId, values, inKey){
          var lst = App.view.get(listId);
          lst.innerHTML="";
          for(var i=0;i < values.length;i++){
             var obj = values[i];
             for(var key in obj){
                 if(key==inKey)
                    lst.innerHTML = lst.innerHTML + "<li id='" + i + "'>" + this.parseHtml(obj[key]) + "</li>";
             } 
          }  
        },
        populateSelect:function(selectId, values, inKey){
            var slct = App.context.document.getElementById(selectId);
            slct.options.length=0;
            for(var i=0;i < values.length;i++){
                var obj = values[i];
                for(var key in obj){
                   if(key==inKey)
                       slct.options[i] = new Option(this.parseHtml(obj[key]), ""+i);
                }
            };
        },
        addUser:function(){
            App.view.get("error").innerHTML = App.controller.addUser(App.view.get("firstName").value,App.view);
        },    
        modifyUser:function(){
            App.view.get("error").innerHTML = App.controller.modifyUser(App.view.get("users").value, undefined, App.view)
        },
        keyPress:function(code){
            if(code===undefined)
                code=window.event;
            if(code!==undefined)
                if(code.charCode==13 || code.keyCode==13)
                    this.addUser();
        },
        listClick:function(li){
            if(li===undefined)li=window.event;
            if(li.target===undefined||li.target===null){
                App.context.window.alert("List selection is not working in this browser.\nSwitching to select/combo object.");
                App.view.get("users").style.display="block";
                App.view.get("list").style.display="none";
                return;
            }
            // select item
            var items = App.view.get("list").getElementsByTagName("li");
            for(var i=0;i < items.length;i++)
                items[i].style.color="#888888";
            li.target.style.color="#17445E";
            // setting selected item
            App.view.get("users").value =li.target.id;
        }
    }
}