
// RFM Software @ 2016

var App = {
    tag:"RFM MVC Skeleton example",
    context:null,    
    
    // Initializing Application
    init:function(context){
                
        // Setting context
        this.context=context;

        //UT controller tests
        App.ut();
         
        // View init
        App.view.init();
        
        // init finished
        this.context.console.log(this.tag + ": init finished! ");

        return true;
     },
    show:function(page){
      if(page=="app")
        App.view=View;
        App.view.init();  
    },
    // MVC - Model pattern reference
    model:Model,
    
    // MVC - Controller pattern reference
    controller:Controller,
    
    // MVC - View pattern reference
    view:View,
    
    ut:function(){
        // UT - testing controller 
        App.view=undefined;
        App.controller.addUser("u");
        console.log("UT: 0 = " + App.model.users.count());
        if(App.model.users.count()!=0)alert("UT ERROR!");
        App.controller.addUser("user1");
        console.log("UT: 1 = " + App.model.users.count());
        if(App.model.users.count()!=1)alert("UT ERROR!");
        App.controller.removeUser(0);
        console.log("UT: 0 = " + App.model.users.count());
        if(App.model.users.count()!=0)alert("UT ERROR!");
        App.controller.addUser("user1");
        App.controller.addUser("user2");
        App.controller.addUser("user3");
        if(App.model.users.count()!=3)alert("UT ERROR!");
        App.controller.modifyUser(1,"user2modif");
        console.log("UT: user2modif = " + App.model.users.get(1).firstName);
        if(App.model.users.get(1).firstName!="user2modif")alert("UT ERROR!");
        console.log("UT: 3 = " + App.model.users.count());
        if(App.model.users.count()!=3)alert("UT ERROR!");
        console.log("UT: no item in collection = " + App.controller.modifyUser(100,"IJIJIJIJ"));
        if("no item in collection" != App.controller.modifyUser(100,"IJIJIJIJ"))alert("UT ERROR!");
        // reset
        App.model.Collection.users=Array();
        App.view=View;        
    }
}

